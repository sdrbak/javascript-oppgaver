/*
 Javascript og mange andre språk operere med scope,
 altså hvor informasjon er tilgjengelig,
 f.eks:
-----------------

 var leet; // globalt scope, kan brukes overalt

 function r(myParam){ // r global scope
   // myParam local scope kun nåes fra mer lokaliserte skop innen for r
   function t(){ // local scope for r
     leet = 1337; // setter globale leet
     return myParam; // returnerer local myParam
   }

   return t; //returnerer t'en som den er når funksjonen blir kjørt
 }

abc =  r("abc")
def = r("def")
def() // "def"
abc() // "abc"

-------

de siste 4 linjene viser noe kalt closure.
Closure er på en måte at noe returnert fra et locale scope
kan huske informasjonen den hadde tilgjengelig.
Altså t funksjonen som blir returnert når man brukte r funksjonen
husker parameteret som var tilgjengelig da den ble returnert selv om den ikke lengre eksisterer inni den funksjonen.
Denne myParam kunne potensielt bli modifisert om funksjon t gjorde noe med den.
Dette lar deg kapsulere informasjon så bare det returnert kan endre på det.

OPPGAVE:
1. Lag en funksjon som kan ta et nummer, og returnerer en funksjon, som vil returnerer et tall som inkrementeres for hver gang du bruker funksjonen.
2. Prøv å bruk bare funksjonen i en for-loop istedet for å lage en vanlig for(var i= 0; i < 5; i++) 
3. Hva er forskjellen på å bruke ++ foran en variabel, istedet for bak en variabel?

 */


