/**
 * Lag en funksjon for å kunne legge til ny heading (h1, h2, h3).
 * Muligheter for hvordan å kjøre koden kan være:
 * funksjon_navn(innhold_for_element, forelderElement, heading_nivå)
 * funksjon_navn(css_selector_for_forelderElement, innhold, heading_nivå)
 *
 * Nyttige methoder:
 * fra document
 *  - getElementById
 *  - getElementByName
 *  - getElementByClassName
 *  - querySelector
 *  - createElement
 *
 * fra elementer:
 * - appendChild
 */
